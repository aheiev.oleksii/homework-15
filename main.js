const characters = "abcdefghijklmnopqrstuvwxyz0123456789";

function generateKey(lengthkey, value) {
  let result = "";
  for (let i = 0; i < lengthkey; i++) {
    result += value[Math.floor(Math.random() * value.length)];
  }

  return result;
}

//  or - але charAt() здається не проходили

// function generateKey(lengthkey, value) {
//   let result = "";

//   for (let i = 0; i < lengthkey; i++) {
//     result += value.charAt(Math.floor(Math.random() * value.length));
//   }

//   return result;
// }

const key = generateKey(50, characters);
console.log(key);
